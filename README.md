Maybe these scripts are useful for other people, too. Feel free to
use/modify. If you use this I am happy to receive an email from you :)


### journal_to_ads  ###

Shell script to replace, e.g., "Astronomy & Astrophysics" with "\aap".
Data is in seperate *.dat file.



### bib_prepare ###

Python script to to the things from *journal_to_ads* and some stuff
more. Simply `./bib_prepare.py -h` yields



    Usgae: ./bib_prepare.py [-h ...] bibtexfile ...
    The following options are available:

       -h         Print this message & exit.
       -j         Convert journal names to abbreviations.
       -r  LIST   Removes fields (expects LIST to be comma seperated)
       -e[f]      Tries to find eprint ID from journal name (if -ef is
                  given, will overwrite ald value)
       -a NAME    Changes arXiv journal name. Use <ID> for eprint id.

Requires [bibtexparser](https://bibtexparser.readthedocs.org).