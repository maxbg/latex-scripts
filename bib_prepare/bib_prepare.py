#!/usr/bin/env python
"""General script to prepare bib files for submission.

For usage run: ./bib_prepare -h

Author: Max Gronke (maxbg+++astro.uio.no)
Date:   23.3.2015 

"""

import bibtexparser
import sys, os, re

def entry_remove_fields(db_entry, fields):
    """Removes fields from bibdb
    """
    for f in fields:
        if f in db_entry:
            db_entry.pop(f)

def entry_journal_abbrev(db_entry):
    """Replaces journal names with their abbreviation command.
    """
    lfix = [['aj', 'Astronomical Journal'],
            ['actaa', 'Acta Astronomica'],
            ['araa', 'Annual Review of Astron and Astrophys'],
            ['apj', 'Astrophysical Journal'],
            ['apj', 'The Astrophysical Journal'],
            ['apjl', 'Astrophysical Journal, Letters'],
            ['apjs', 'Astrophysical Journal, Supplement'],
            ['ao', 'Applied Optics'],
            ['apss', 'Astrophysics and Space Science'],
            ['aap', 'Astronomy & Astrophysics'],
            ['aapr', 'Astronomy and Astrophysics Reviews'],
            ['aaps', 'Astronomy and Astrophysics, Supplement'],
            ['azh', 'Astronomicheskii Zhurnal'],
            ['baas', 'Bulletin of the AAS'],
            ['caa', 'Chinese Astronomy and Astrophysics'],
            ['cjaa', 'Chinese Journal of Astronomy and Astrophysics'],
            ['icarus', 'Icarus'],
            ['jcap', 'Journal of Cosmology and Astroparticle Physics'],
            ['jrasc', 'Journal of the RAS of Canada'],
            ['memras', 'Memoirs of the RAS'],
            ['mnras', 'Monthly Notices of the RAS'],
            ['mnras', 'Monthly Notices of the Royal Astronomical Society'],
            ['na', 'New Astronomy'],
            ['nar', 'New Astronomy Review'],
            ['pra', 'Physical Review A: General Physics'],
            ['pra', 'Physical Review A'],
            ['prb', 'Physical Review B: Solid State'],
            ['prb', 'Physical Review B'],
            ['prc', 'Physical Review C'],
            ['prd', 'Physical Review D'],
            ['pre', 'Physical Review E'],
            ['prl', 'Physical Review Letters'],
            ['pasa', 'Publications of the Astron. Soc. of Australia'],
            ['pasa', 'Publications of the Astronomical Society of Australia'],
            ['pasp', 'Publications of the ASP'],
            ['pasj', 'Publications of the ASJ'],
            ['rmxaa', 'Revista Mexicana de Astronomia y Astrofisica'],
            ['qjras', 'Quarterly Journal of the RAS'],
            ['skytel', 'Sky and Telescope'],
            ['solphys', 'Solar Physics'],
            ['sovast', 'Soviet Astronomy'],
            ['ssr', 'Space Science Reviews'],
            ['zap', 'Zeitschrift fuer Astrophysik'],
            ['nat', 'Nature'],
            ['iaucirc', 'IAU Cirulars'],
            ['aplett', 'Astrophysics Letters'],
            ['apspr', 'Astrophysics Space Physics Research'],
            ['bain', 'Bulletin Astronomical Institute of the Netherlands'],
            ['fcp', 'Fundamental Cosmic Physics'],
            ['gca', 'Geochimica Cosmochimica Acta'],
            ['grl', 'Geophysics Research Letters'],
            ['jcp', 'Journal of Chemical Physics'],
            ['jgr', 'Journal of Geophysics Research'],
            ['jqsrt',
             'Journal of Quantitiative Spectroscopy and Radiative Transfer'],
            ['memsai', 'Mem. Societa Astronomica Italiana'],
            ['nphysa', 'Nuclear Physics A'],
            ['physrep', 'Physics Reports'],
            ['physscr', 'Physica Scripta'],
            ['planss', 'Planetary Space Science'],
            ['procspie', 'Proceedings of the SPIE'],
            ['aap', 'Astronomy and Astrophysics']]
    for abbrev, name in lfix:
        if 'journal' in db_entry:
            if db_entry['journal'].strip() == name.strip():
                db_entry['journal'] = '\\' + abbrev

def fill_eprint_id(ds, overwrite = False):
    """Tries to fill in eprint id from journal name (if not set).    
    """
    if 'journal' not in ds or (not overwrite and 'eprint' in ds):
        return
        
    journal = ds['journal']
    if "arxiv" in journal.lower() or "preprint" in journal.lower():
        m = re.findall("arXiv:[0-9]{4}\.[0-9]{4,5}", journal)
        if len(m) == 0: # not found, try to find in link
            if "arxiv.org" in ds['link']:
                m = re.findall("[0-9]{4}\.[0-9]{4,5}",ds['link'])
            if len(m) == 0:
                return
            aid = "arXiv:" + m[0]
        else:
            aid = m[0]
        # aid = m[0][6:] # just the number

        ds['eprint']  = aid
    


def entry_fix_arxiv_journal(ds, aj):
    """Fixes journal name for arxiv entries.
    
    Keyword Arguments:
    ds  -- Data Set
    aj  -- New journal string. <ID> will be replaced by eprint it.
    """
    if'journal' not in ds or ('eprint' not in ds and "<ID>" in aj):
        return
        
    journal = ds['journal']
    if "arxiv" not in journal.lower() and "preprint" not in journal.lower():
        return

    if "<ID>" in aj:
        aid = ds['eprint']
        ds['journal'] = aj.replace("<ID>", aid)
    else:
        ds['journal'] = aj

    
    

def print_usage():
    print "Usgae: ./bib_prepare.py [-h ...] bibtexfile ..."
    print "The following options are available:"
    print "   -h         Print this message & exit."
    print "   -j         Convert journal names to abbreviations."
    print "   -r  LIST   Removes fields (expects LIST to be comma seperated)"
    print "   -e[f]      Tries to find eprint ID from journal name (if -ef is "
    print "              given, will overwrite ald value)"
    print "   -a NAME    Changes arXiv journal name. Use <ID> for eprint id."
    print "\n"
    sys.exit(0)



def main():
    if len(sys.argv) <= 1:
        print_usage()

    remove_fields = None
    fix_eprint = 0
    fix_journal = False
    fix_arxiv_journal = False
    for i, arg in enumerate(sys.argv):
        if arg[0] != '-':
            continue
        if arg == '-h':
            print_usage()
        elif arg == '-r':
            remove_fields = sys.argv[i+1].split(",")
        elif arg == '-e':
            fix_eprint = 1
        elif arg == '-ef':
            fix_eprint = 2
        elif arg == '-j':
            fix_journal = True
        elif arg == '-a':
            fix_arxiv_journal = sys.argv[i+1]

            
            
    bibfile = sys.argv[-1]

    try:
        f = open(bibfile, "r")
        f.close()
    except:
        print "Cannot read file \"%s.\"" %(bibfile)
        sys.exit(-1)

    with open(bibfile) as bibtex_file:
        bib_database = bibtexparser.load(bibtex_file)

    # main loop
    for ds in bib_database.entries:
        if remove_fields is not None:
            entry_remove_fields(ds, remove_fields)

        if fix_journal:
            entry_journal_abbrev(ds)

        if fix_eprint:
            fill_eprint_id(ds, overwrite = (fix_eprint == 2))

        if fix_arxiv_journal:
            entry_fix_arxiv_journal(ds, fix_arxiv_journal)
            
    # output
    bibtex_str = bibtexparser.dumps(bib_database)

    print bibtex_str.encode('ascii', 'ignore')
    
if __name__ == "__main__":
    main()

    
