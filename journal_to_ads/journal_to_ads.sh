#!/bin/bash
#
# Converts Journal names in bib file to its aas macros
# http://doc.adsabs.harvard.edu/abs_doc/aas_macros.html
# maxbg (attt) astro.uio (dot) no, 2013-05-31
#
# Usage: ./journal_to_ads.sh FILE.bib
#
#

datfile=~/bin/journal_to_ads.dat

sedstr=""

while read line; do
    if [[ "$sedstr" != "" ]]; then
        sedstr=$sedstr";"
    fi
    search=`echo $line|awk -F';' ' { print($2) }'`
    replace=`echo $line|awk -F';' ' { print($1) }'`
    #echo "$search --> $replace"
    sedstr=$sedstr"s|{$search}|\{\\\\$replace\}|g"
done < $datfile

sed  -e "$sedstr" $1
